import {AzureADServiceConstants} from './ngAuth/authenticators/AzureADServiceConstants';

export const serviceConstants: AzureADServiceConstants = new AzureADServiceConstants(
    'c9754a4f-5be9-4b62-a79b-31f59137b0af',
    'common',
    'http://localhost:53986/Onboarding/ProcessCode',
    'https://graph.windows.net'
);
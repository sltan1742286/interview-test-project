import { Component, ViewEncapsulation, NgModule, HostBinding } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import { Observable, Subscription } from 'rxjs/Rx';

@Component({
    selector: 'overview',
    providers: [],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './overview.template.html'
})
export class OverviewComponent {
    @HostBinding('attr.id') get get_id() { return "overview"; }
    @HostBinding('class') get get_class() { return "overview"; }

    constructor() {
    }
        
    ngOnDestroy() {
    }
}

@NgModule({
    declarations: [
        OverviewComponent
    ],
    imports: [
        CommonModule
    ]
})

export class OverviewModule {}
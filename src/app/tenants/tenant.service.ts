
import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


import { Tenant } from './tenant';

@Injectable()
export class TenantService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private tenantesUrl = 'http://localhost:53986/api/Tenants';  // URL to web api

  constructor(private http: Http) { 
      
  }

  getTenantes(): Promise<Tenant[]> {
    return this.http.get(this.tenantesUrl, {withCredentials:true})
               .toPromise()
               .then(response => response.json() as Tenant[])
               .catch(this.handleError);
               
  }

  getTenant(id: number): Promise<Tenant> {
    const url = `${this.tenantesUrl}/${id}`;
    return this.http.get(url, {withCredentials:true})
      .toPromise()
      .then(response => response.json().data as Tenant)
      .catch(this.handleError);
  }



  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}


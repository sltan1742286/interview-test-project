import { NgModule, Component, OnInit } from '@angular/core';
import { MdListModule, MdDialog } from '@angular/material';

import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { Tenant } from './tenant';
import { TenantService } from './tenant.service';

import { EditorTenantDialog } from './editor-tenant.dialog';

@Component({
    selector: 'tenants',
    templateUrl: './tenants.template.html'
})
export class TenantsComponent implements OnInit {
    tenants: Tenant[];
    selectedTenant: Tenant;
    constructor(
        private tenantService: TenantService,
        public dialog: MdDialog) { }

    ngOnInit(): void {
        this.getTenantes();
    }

    getTenantes(): void {
        this.tenantService
            .getTenantes()
            .then(tenants => this.tenants = tenants);

            
    }


    onAdd() {
        this.dialog.open(EditorTenantDialog);
    }
}

@NgModule({
    declarations: [
        TenantsComponent
    ],
    imports: [
        CommonModule,
        MdListModule
    ],
    providers: [
        TenantService
    ]
})

export class TenantsModule { }

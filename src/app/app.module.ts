import { MaterialModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { APP_ROUTING } from './app.routes';

import { serviceConstants } from './authsettings.config';

import { CookieService } from 'angular2-cookie/services/cookies.service';
//import { AzureADAuthService } from './ngAuth/authenticators/AzureADAuthService';
//import { AuthenticatedHttpService } from './ngAuth/AuthenticatedHttpService';
import { GraphService } from './services/graph.service';
import { AuthGuardService } from './auth/auth-guard.service';


import { AppComponent } from './app.component';
import { ToastComponent } from './toast/toast.component';
import { NoContent } from './no-content';
import { ToolbarComponent } from './toolbar';

import { SigninModule } from './signin';
import { TenantsModule } from './tenants';
import { OverviewModule } from './overview';

//var authenticator = new AzureADAuthService(serviceConstants);

const APP_PROVIDERS = [
    CookieService
];

@NgModule({
    bootstrap: [
        AppComponent
    ],
    declarations: [
        AppComponent,
        NoContent,
      
      
        ToolbarComponent
    ],
    imports: [
        SigninModule,
        OverviewModule,
        TenantsModule,
        
          BrowserModule,

    ReactiveFormsModule,
   

        MaterialModule,
        
        FormsModule,
        HttpModule,

        APP_ROUTING
    ],
    providers: [
        APP_PROVIDERS,
        AuthGuardService,
        GraphService, 
        ToastComponent
         
  /*   AuthenticatedHttpService,
        {
            provide: AzureADAuthService,
            useValue: {} 
        }*/
    ]
})
export class AppModule {}

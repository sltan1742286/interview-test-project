"use strict";
var core_1 = require('@angular/core');
var NoContent = (function () {
    function NoContent() {
    }
    NoContent = __decorate([
        core_1.Component({
            selector: 'no-content',
            template: "\n        <div>\n            <h1>404: page missing</h1>\n        </div>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], NoContent);
    return NoContent;
}());
exports.NoContent = NoContent;
//# sourceMappingURL=no-content.js.map
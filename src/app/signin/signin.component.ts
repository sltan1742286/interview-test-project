import { Component, ViewEncapsulation, NgModule, HostBinding, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '@angular/material';

import { Router } from '@angular/router';


import { GraphService } from "../services/graph.service";

@Component({
    selector: 'signin',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './signin.component.html',
    styleUrls: ['signin.component.css']
})
export class SigninComponent {
    constructor(private graphService: GraphService,
        private router: Router) {}

    public onOfficeLoginClick() {
      //  this.router.navigate(["/office"])
        this.graphService.signIn('/');
    }
}

@NgModule({
    declarations: [
        SigninComponent
    ],
    imports: [
        CommonModule,
        MaterialModule
    ],
    providers: [
        GraphService
    ]
})

export class SigninModule {}
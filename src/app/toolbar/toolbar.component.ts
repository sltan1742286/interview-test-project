import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthGuardService } from "../auth/auth-guard.service";

@Component({
    selector: 'toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: [ './toolbar.component.css' ]
})
export class ToolbarComponent implements OnInit {
  
    constructor(private router: Router, private authGuardService: AuthGuardService) {
      
    }

    private isActive() {
        return this.router.url === '/signin' ? false : true;
    }

    private onSignOutClick() {
        this.authGuardService.signOut();
    }

    ngOnInit(): void {
       
    }
}
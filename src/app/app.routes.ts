import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoContent } from './no-content';
import { TenantsComponent } from './tenants';
import { SigninComponent } from './signin';
import { OverviewComponent } from './overview';

import { AuthGuardService } from './auth/auth-guard.service';

export const appRoutes: Routes = [
    { path: 'signin',  component: SigninComponent },
    { path: 'overview', component: OverviewComponent, canActivate: [AuthGuardService] },
    { path: 'tenants', component: TenantsComponent, },
    { path: '**', component: TenantsComponent }
];

export const APP_ROUTING: ModuleWithProviders = RouterModule.forRoot(appRoutes);
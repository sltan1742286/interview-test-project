import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { GraphService } from './services/graph.service';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: [
      './app.component.css'
  ]
})
export class AppComponent implements OnInit {
    title = 'CiraSync';

    constructor(private router: Router, private authGuardService: GraphService) { }

    private isActive() {
        return this.router.url === '/signin' ? false : true;
    }

    ngOnInit(): void {
    }
}

export const AZURE_CONFIG = {
  authorityHostUrl: "https://login.windows.net",
  clientId: "c9754a4f-5be9-4b62-a79b-31f59137b0af",
  disableRenewal: true,
  endpoints: {
    graphApiUri: "https://graph.microsoft.com",
  },
  extraQueryParameter: "nux=1",
  postLogoutRedirectUri: "",
  redirectUri:  "http://localhost/callback",
  resource: "https://graph.microsoft.com",
  tenant: "common",
};

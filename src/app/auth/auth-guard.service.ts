﻿import { Injectable } from '@angular/core';
import {
    CanActivate,
    Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
} from '@angular/router';

import { GraphService } from '../services/graph.service';
import { ToastComponent } from '../toast/toast.component';
import { USER_MESSAGES } from '../messages/messages';

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(private graphService: GraphService, private toast: ToastComponent, private router: Router) {
       
     }

    public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
      
        return this.graphService.isUserAuthenticated().then(() => {
            return true;
        }).catch((err) => {
            console.log(err);
           // this.toast.show(USER_MESSAGES.not_authenticated + " " + err);
            this.router.navigate(['signin']);
            return false;
        });
    }

    public isActive()  {
      
      this.graphService.isAuth();
       
    }

     public signIn() {
        
        this.graphService.signIn();
    }

     public signOut() {
        this.graphService.signOut();
    }
}

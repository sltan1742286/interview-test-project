import { Westward.AdminPage } from './app.po';

describe('westward.admin App', () => {
  let page: Westward.AdminPage;

  beforeEach(() => {
    page = new Westward.AdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
